var gulp = require('gulp');

var concat  = require('gulp-concat');
var uglify  = require('gulp-uglify');
var stylus  = require('gulp-stylus');
var htmlmin = require('gulp-htmlmin');
var rupture = require('rupture');
var nib = require('nib');

//DEFINE TASKS
//============
//js task
gulp.task('js', function() {
  //source files
  gulp.src('js/**/*.*')
   //proccess
  .pipe(concat('build.js'))
  .pipe(uglify())
  //files dest
  .pipe(gulp.dest('dist/js'))
});

//Stylus task
gulp.task('style', function() {
  gulp.src('styles/index.styl')
  .pipe(stylus({
    //compress stylus
    compress: true,
    use: [
      nib(),
      rupture(),
    ],
    }))
  .pipe(concat('main.css'))
  .pipe(gulp.dest('dist/'));
})

gulp.task('style_blog', function() {
  gulp.src('styles/blog.styl')
  .pipe(stylus({
    //compress stylus
    compress: true,
    use: [
      nib(),
      rupture(),
    ],
    }))
  .pipe(concat('blog.css'))
  .pipe(gulp.dest('dist/'));
})


gulp.task('style_post', function() {
  gulp.src('styles/post.styl')
  .pipe(stylus({
    //compress stylus
    compress: true,
    use: [
      nib(),
      rupture(),
    ],
    }))
  .pipe(concat('post.css'))
  .pipe(gulp.dest('dist/'));
})



//HTML Task
gulp.task('html', function(){
  gulp.src('index.html')
  .pipe(htmlmin({
    collapseWhitespace: true,
    removeComments: true,
    minifyURLs: true,
    minifyJS: true
    }))
  .pipe(gulp.dest('dist'));
});


gulp.task('html_blog', function () {
  gulp.src('blog.html')
  .pipe(htmlmin({
    collapseWhitespace: true,
    removeComments: true,
    minifyURLs: true,
    minifyJS: true
    }))
  .pipe(gulp.dest('dist'));
})

gulp.task('html_post', function () {
  gulp.src('post.html')
  .pipe(htmlmin({
    collapseWhitespace: true,
    removeComments: true,
    minifyURLs: true,
    minifyJS: true
    }))
  .pipe(gulp.dest('dist'));
})


gulp.task('html_blog_header', function () {
  gulp.src('blog-header.html')
  .pipe(htmlmin({
    collapseWhitespace: true,
    removeComments: true,
    minifyURLs: true,
    minifyJS: true
    }))
  .pipe(gulp.dest('dist'));
})

//Run tasks when do changes in files
gulp.task('watch', function(){
  gulp.watch('js/**/*.*',['js']);
  gulp.watch('styles/**/*.*',['style', 'style_blog', 'style_post']);
  gulp.watch('index.html',['html']);
  gulp.watch('blog.html',['html_blog']);
  gulp.watch('blog-header.html',['html_blog_header']);
  gulp.watch('post.html',['html_post']);
});

//Default task
gulp.task('default', ['watch']);

//join tasks
gulp.task('minify', ['html', 'style', 'js']);